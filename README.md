# AustinCloudGuru Cloudformation Templates

## Purpose
This repository contains various CloudFormation templates that I have written for various projects I have worked on.  The basic design that I use is a layered approach, using each template as a building block for the overall infrastructure.  Everything starts with the aws-base template, and the remaining templates all build off it.

Another principle is to try to have each template be as customizable as possible for individual environments.  For example, the base script can create between 0 and 3 public subnets (and the associated NATs & Route entries) so that you can have a full blown redundant architecture in production while reducing the number of subnets in lower environments (saving costs).

## Templates

### Aws Base Enviornment (aws-base)
This template creates a base environment inside of AWS.  It creates the VPC, an InternetGateway, and the Route53 Hosted Zone by default.  Additionally, it can create between 1 and 3 public subnets, and the associated NATs and routing tables necessary to support the subnet.

#### Parameters

| Parameter | Description | Default |
|-----|-----|-----|
| VpcName | Name for the VPC | aws_vpc |
| VpcCidr | The CIDR range for the VPC | 10.0.0.0/16 |
| CreateSubnet0 | True/False whether to create the first public subnet | false |
| PublicSubnet0Range | The CIDR subnet for the first public subnet| 10.1.1.0/24 |
| CreateSubnet1 | True/False whether to create the second public subnet | false |
| PublicSubnet1Range | The CIDR subnet for the second public subnet| 10.1.2.0/24 |
| CreateSubnet2 | True/False whether to create the third public subnet | false |
| PublicSubnet2Range | The CIDR subnet for the third public subnet| 10.1.3.0/24 |
| HostedZoneName | The FQDN for the Hosted Zone| example.com |

#### Launch Command

Launch via Console:

[![](https://s3.amazonaws.com/cloudformation-examples/cloudformation-launch-stack.png)](https://console.aws.amazon.com/cloudformation/home?region=us-east-1#/stacks/new?stackName=aws-base&templateURL=https://s3.amazonaws.com/austincloud-cf-templates/aws-base.template)

Launch via CLI:

    aws cloudformation create-stack --profile {AWS Profile Name} --stack-name {Stack Name} --template-url "https://s3.amazonaws.com/austincloud-cf-templates/aws-base.template" --parameters file:///localpath/to/custom-parameters.json

### AWS Application Subnets (aws-app-subnets)
This template builds on the aws-base by creating between 1 and 3 private subnets (it should match the number defined in aws-base).

#### Parameters

| Parameter | Description | Default |
|-----|-----|-----|
| AppSubnet0Range | The CIDR subnet for the first public subnet| 10.2.1.0/24 |
| AppSubnet1Create | True/False whether to create the second public subnet | false |
| AppSubnet1Range | The CIDR subnet for the second public subnet| 10.2.2.0/24 |
| AppSubnet2Create | True/False whether to create the third public subnet | false |
| AppSubnet2Range | The CIDR subnet for the third public subnet| 10.2.3.0/24 |

#### Launch Command

Launch via Console:

[![](https://s3.amazonaws.com/cloudformation-examples/cloudformation-launch-stack.png)](https://console.aws.amazon.com/cloudformation/home?region=us-east-1#/stacks/new?stackName=aws-app-subnets&templateURL=https://s3.amazonaws.com/austincloud-cf-templates/aws-app-subnets.template)

Launch via CLI:

    aws cloudformation create-stack --profile {AWS Profile Name} --stack-name {Stack Name} --template-url "https://s3.amazonaws.com/austincloud-cf-templates/aws-app-subnets.template" --parameters file:///localpath/to/custom-parameters.json

### Static S3 Website (static-s3-website)
This template creates an S3 based website that is generated using Jekyll.  It creates S3 Buckets, a CodePipeline & CodeBuild Job to generate the Jekyll code, all the related roles & policies and creates a CloudFront Distribution for the site.  When the CloudFormation template creates the SSL certificate, it will send an email for approval to the email of record for the domain and will not continue until it has been approved.

It requires a GitHub repository that contains the Jekyll code and a buildspec.yml file with the following contents:

    version: 0.1

    phases:
      install:
        commands:
          - gem install jekyll jekyll-paginate jekyll-sitemap jekyll-gist
      build:
        commands:
          - echo "******** Building Jekyll site ********"
          - jekyll build
          - echo "******** Uploading to S3 ********"
          - aws s3 sync _site/ s3://{ Bucket Name }


#### Parameters

| Parameter | Description | Default |
|-----|-----|-----|
| Domain Name | The domain name for the website | example.com |
| GitHubOwner | The GitHub Repository User |  |
| GitHubRepo | Repo that contains the Jekyll Code|  |
| GitHubBranch | Branch that will trigger build/deploy | master |
| GitHubOauthToken | GitHub Personal Access Token|  |

#### Launch Command

Launch via Console:

[![](https://s3.amazonaws.com/cloudformation-examples/cloudformation-launch-stack.png)](https://console.aws.amazon.com/cloudformation/home?region=us-east-1#/stacks/new?stackName=static-s3-website&templateURL=https://s3.amazonaws.com/austincloud-cf-templates/static-s3-website.template)

Launch via CLI:

    aws cloudformation create-stack --profile {AWS Profile Name} --stack-name {Stack Name} --template-url "https://s3.amazonaws.com/austincloud-cf-templates/static-s3-website.template" --parameters file:///localpath/to/custom-parameters.json


## License

MIT
